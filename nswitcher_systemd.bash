#!/usr/bin/env bash
[[ "${BASH_SOURCE:0:1}" = '.' ]] && nswitcherDirectory="$PWD" || nswitcherDirectory="${BASH_SOURCE%/*}"

unset prerequisiteMissing
[[ -e ${nswitcherDirectory}/nswitcher_user_variables.bash ]] || { echo ' Missing nswitcher_user_variables.bash' ; prerequisiteMissing='yes' ; }
[[ -e ${nswitcherDirectory}/nswitcher.bash ]] || { echo ' Missing nswitcher.bash' ; prerequisiteMissing='yes' ; }
[[ -e ${nswitcherDirectory}/route_up_options.bash ]] || { echo ' Missing route_up_options.bash (needed for nftables firewall).' ; prerequisiteMissing='yes' ; }

[[ $prerequisiteMissing ]] && return

[[ $1 = install ]] && {
readarry -t nswitcherService << writeservice 
[Unit]
Description=openvpn-switcher
Wants=network-online.target

[Service]
Type=forking
ExecStart=bash -c 'source ${nswitcherDirectory}/nswitcher.bash ; nswitch random nmanager vpnroute firewall'

[Install]
WantedBy=multi-user.target
writeservice

[[ -d /etc/systemd/system ]] && { printf '%s\n' $nswitcherService > /etc/systemd/system/nswitcher_random_vpn.service
sudo systemctl daemon-reload
sudo systemctl enable nswitcher_random_vpn.service ; }
}

[[ $1 = uninstall ]] && {
systemctl stop nswitcher_random_vpn
systemctl disable nswitcher_random_vpn
rm /etc/systemd/system/nswitcher_random_vpn.service
systemctl daemon-reload
systemctl reset-failed
}

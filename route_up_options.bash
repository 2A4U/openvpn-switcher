#!/usr/bin/env bash
[[ "${BASH_SOURCE:0:1}" = '.' ]] && nswitcherDirectory="$PWD" || nswitcherDirectory="${BASH_SOURCE%/*}"
vpntmpDirectory="${vpntmpDirectory:-/dev/shm}"

[[ $@ == *'writePublicAddress'* ]] && { readarray -t allRoutes <<< $(route --numeric)
for publicAddress in "${!allRoutes[@]}" ; do
[[ "${allRoutes[publicAddress]}" == *UGH*e* ]] && { publicAddress="${allRoutes[publicAddress]%%[[:space:]]*}"
[[ $@ == *'showIP'* ]] && echo -e " \xE2\x98\x82 ${publicAddress}"
[[ $@ == *'autoshiftOn'* ]] && ( umask 0066 ;  echo $publicAddress > ${vpntmpDirectory}/public_address.log ) ; break ; } || unset publicAddress
done ; }

[[ $@ == *'firewallOn'* && "$publicAddress" ]] && { readarray -t evaluateIP < ${nswitcherDirectory}/rules.nft
( umask 0066 ; printf '%s\n' "${evaluateIP[@]/currentVpnIP/${publicAddress}}" > ${vpntmpDirectory}/rules.nft )

sudo nft --file "${vpntmpDirectory}/rules.nft" && echo -e ' \xe2\x9c\x93 Firewall \e[0;36mon\e[0m.' ; unlink "${vpntmpDirectory}/rules.nft" &
}
[[ $@ == *'cutDefaultRoute'* ]] && { readarray -t -n 1 defaultRoute <${vpntmpDirectory}/default_route.log
sudo ip route del ${defaultRoute} && echo -e ' \xe2\x9C\x82 Ethernet route \e[0;36mcut\e[0m.'
}
unset publicAddress allRoutes evaluateIP defaultRoute vpntmpDirectory

# openvpn-switcher

`nswitch` loops through *\*.ovpn* files listed in directories. New *\*.ovpn* files are created in a temporary directory. Each directory can contain a unique encrypted *vpn_login.txt* or use a default *vpn_login.txt* or none at all.

## Getting Started

1. `git clone https://gitlab.freedesktop.org/2A4U/openvpn-switcher` to any directory (without spaces), for example, $HOME.

2. Edit '$HOME/openvpn-switcher/*nswitcher_user_variables.bash.**template***', then save as '*nswitcher_user_variables.bash*'.

3. Add following line to $HOME/*.bashrc* or $HOME/*.bash_profile*: `source $HOME/openvpn-switcher/nswitcher.bash`

4. `cd $HOME/openvpn-switcher ; chmod u+x *.bash` - make scripts executable.

5. `nswitch`

## `openvpn` Prerequisites
* [update-resolv-conf.sh](https://github.com/alfredopalhares/openvpn-update-resolv-conf) - SysVinit -- fixes OpenVPN issue of connecting to a VPN but not able to resolve IP addresses to web addresses.
* [update-systemd-resolved](https://github.com/jonathanio/update-systemd-resolved) - systemd Dbus DNS resolver
* $USER with sudo access.

## `nmcli` Prerequisites
* [NetworkManager-ovpn](https://gitlab.gnome.org/GNOME/NetworkManager-openvpn) - `sudo apt-get install network-manager-openvpn`
* `sudo groupadd -g 320 nm-openvpn` and `sudo useradd -u 320 -g nm-openvpn -d /var/lib/openvpn/chroot -s /bin/false nm-openvpn` solves error message returned from `grep "User 'nm-openvpn' not found, check NM_OPENVPN_USER" /var/log/dmesg`.

## Example Uses

###### To cycle through all *\*.ovpn* files in a directory type:
```
cd ~/vpndirectory ; nswitch
```
or
```
nswitch ~/vpndirectory
```
then use keyboard shortcut, <kbd>crtl</kbd>+<kbd>c</kbd> to close OpenVPN and then repeat `nswitch ~/vpndirectory` or `nswitch` if current directory contains *\*.ovpn* files.

###### To use a specific *\*.ovpn* file type:
```
nswitch ~/vpndirectory/abc.ovpn #note: has forward slash '/' before 'abc'
```
or
```
nswitch ~/vpndirectory abc.ovpn #note: no forward slash '/' before 'abc'
```
or
```
cd ~/vpndirectory ; nswitch abc.ovpn
```
or by index number:
```
nswitch 1
```
###### To restart from the newest *\*.ovpn* file type:
```
nswitch restart
```
###### To send your default login info in $HOME/openvpn-switcher/*vpn_login.txt* to a *\*.ovpn* file in a directory which does not have a *vpn_login.txt* type:
```
nswitch login
```
or
```
nswitch ~/vpndirectory/subdirectory/childdirectory/abc.ovpn login
```

## Additional Commands

command|description
-|-
`remove` `delete`|delete last used *\*.ovpn*
`restart` `new` `reset`|restart from the newest *\*.ovpn* file by date — equivalent to `nswitch 1`
`killvpn`¹ `kill`|terminate all running instances of OpenVPN
`skip` `next`|skip the next *\*.ovpn* file in queue
`reconnect` `hold`|do not switch to the next *\*.ovpn* file from queue instead reuse previous connection
`random` `r`|select a pseudorandom *\*.ovpn* file from queue directory
`limit` `minus` `l`|remove subsequent chance to reconnect to previous *\*.ovpn* — use with `random` — does not require `save`
`resume`|restore queue with $connectLogDirectory/*connections_remaining$PWD.log* — requires log from `save`
`save` `savelog` `s`|save remaining connections to $connectLogDirectory — use with `limit`, or when switching directories
`background`⁴ `bg` `auto` |allow console login after autostart — spawn background process — `auto` alias depreciated 
`purge`¹ ²|remove `nswitch` traces
`nmanager`³ `nm`|switch using `nmcli` — reload function moved to `nmreload` option
`bindsys` `nmshm`|bind /etc/NetworkManager/system-connections to /dev/shm
`syspass`³|write decrypted password to *\*.nmconnection* — invokes `bindsys`
`link`|connect to new *\*.ovpn* with current connection
`latency`|connect by ascending latency — invokes `resume` `save` `limit` — log online hosts<br/>`latency random` - connect to pseudorandom host with [≊](https://en.wikipedia.org/wiki/%E2%89%8A) minimum latency
`nologhost` `nl`|do not log online hosts after connection scan
`loghostdown`² `lh`| log offline hosts — use before `nswitch my_ovpn_directory deletehosts`
`deletehosts`²| delete offline hosts' original *\*.ovpn* — use after `nswitch my_ovpn_directory loghostdown`
`applist` `kl`|before switching, kill apps listed on command line or ${nswitcherDirectory}/app_list.txt
`nobody`⁴ `nogroup` `dp`|drop root privilages after `openvpn` starts
`nonpersistent` `np`|removes 'persist-tun', 'persist-key' and 'resolv-retry' in *\*.ovpn*
`noresolvretry`⁴ `nr`|disable infinite retries when hostname resolve fails — use with `nonpersistent`
`direct`¹ ²|VPN down — direct connection
`vpnroute`⁵ `dr`|delete default route after connection
`firewall`⁵ `fw`|nftables firewall — [kernel requirements](https://wiki.nftables.org/wiki-nftables/index.php/Building_and_installing_nftables_from_sources#Installing_Linux_kernel_with_nftables_support)
`ipv6`¹ ²|show ipv6 status — running as root: `ipv6 off` - disable ipv6 ; `ipv6 on` - enable ipv6<br/>syntax: `ipv6 ; nswitch`
`autoshift`¹ ³|scan with `nmap -Pn -sn` then `nswitch` when connection is not found — break loop at 10 `nswitch` attempts<br/>syntax: `autoshift my_ovpn_directory my_options nmanager`
`breakautoshift`³|break scan loop after connection — use at system boot to allow login
`retry`³|invokes `reconnect` — retries are limited to 9<br/>syntax: `autoshift my_ovpn_directory my_options nmanager retry 1`
`scanhost`³|check if host is up with `nmap --disable-arp-ping -Pn -p${port[@]}` instead of default `ping` in `autoshift`<br/>syntax: `autoshift my_ovpn_directory my_options nmanager scanhost`
`nmreload`¹ ³|reload active ethernet or make ethernet default to enable `nmcli` connections — need varies by system
`showip` `ip`|show ip after route is up
`max`³|overrides `autoshift` and `retry` default (9) connect reattempts — see also $tryLimit in *nswitcher_user_variables.bash.template*<br/>syntax:`autoshift max 1`
`udp`|set connection 'proto' to [udp](https://duckduckgo.com/?q=udp+vs+tcp)
`tcp`|set connection 'proto' to tcp

\* New or modified option.<br/>
¹ Can be run on console without `nswitch` prefixed.<br/>
² Runs without switching.<br/>
³ `nmcli` only.<br/>
⁴ `openvpn` only.<br/>
⁵ Prevent IP leak.

## Warnings
* $vpntmpDirectory/{*vpn_login.txt*,*nmlogin.txt*} exists for ~1 second, decrypted from $ovpnConfigDirectory/*vpn_login.txt.**gpg***
* `nswitch` restores the default route in-between switching unless the `link` option is used.
* `nswitch` disables nftables in-between switching.
* `autoswitch applist my_app_name` or `autoshift applist my_app_name`may cause loss of data if a VPN server goes offline. For example, if `autoswitch` detects a VPN failure, `applist firefox`will close Firefox (when using a GitLab web application during an update of a git repository) before the next VPN is connected.

## Notes and Behavior
* Lists are ordered by date.
* `nswitch` creates a new *\*.ovpn* file (set with read/write permissions only for $USER) in $vpntmpDirectory with login info derived from $vpnLoginTxt found in $vpnLoginTextDirectory or the $vpnLoginTxt from the directory in which the *\*.ovpn* file was loaded from. The original *\*.ovpn* is left unedited.
* `nswitch` converts Windows end-of-line encoding to Linux end-of-line encoding.
* Using an index number beyond the total number of *\*.ovpn* files in a directory will cause `nswitch` to restart from the newest file.
* `limit random` will not reuse a connection until *\*.ovpn* list has been exhausted.
* `limit` will invoke `resume` if a directory change is detected between 2 `nswitch` commands, e.g.: `nswitch dir1 limit ; nswitch dir2 limit`.
* `bindsys` copies /etc/NetworkManager/system-connections/ to /dev/shm before `mount --bind`.
* `nmanager syspass` invokes `bindsys` so that login info cannot be retrieved by disk recovery tools.
* `nmanager` sets to default an active ethernet connection only.
* `vpnroute` — a default ethernet connection is not established in case the current vpn is disconnected.

## Tips
* Specify a $userDefaultOvpnConfigDirectory location in $HOME/openvpn-switcher/*nswitcher_user_variables.bash* and nswitcher will use *\*.ovpn* files from that location if no *\*.ovpn* files are found at the current working directory (echo $PWD). In other words, `nswitch` will connect from any location.
* Copy a *vpn-login.txt* to the directory containing your *\*.ovpn* files to avoid manual login.
* To prevent accidental closure of a window running OpenVPN navigate to and run nswitcher from a virtual terminal (<kbd>ctrl</kbd>+<kbd>alt</kbd>+<kbd>F6</kbd> `nswitch` for example).
* Add `if ! pidof openvpn &> /dev/null ; then nswitch ; fi` to $HOME/*.bash_profile* to start OpenVPN if it is not running when logging in from a virtual terminal.
* Quick switching alias in *.bashrc*: `alias ns='source /my_source_directory/nswitcher.bash ; nswitch my_ovpn_directory resume save limit random nmanager vpnroute && exit'`
* To delete, after `reboot` or `shutdown`, NetworkManager VPN connection logs in *messages* add the following line to a startup file (for example rc.local): `[[ -f /var/log/messages ]] && { rm /var/log/messages ; ln -s /dev/shm/messages /var/log/messages ; }`

## Remember connection queue between reboots
Change $connectLogDirectory specified in file *nswitcher_user_variables.bash.template* to point to non-volatile storage.

## Prevent IP leak if VPN goes offline
DHCP may restore a default route after `nswitch vpnroute` deletes it. Steps to prevent default route recreaton:
1. [Get default gateway ip](https://gitlab.freedesktop.org/2A4U/openvpn-switcher#get-default-gateway-ip-info)
2. [Add "nogateway" to dhcpcd.conf](https://gitlab.freedesktop.org/2A4U/openvpn-switcher#stopping-dhcp-from-readding-a-default-gateway-during-vpn-connection)
3. Disabling the default gateway leaves no network connection on system start. Therefore [add to *rc.local*](https://gitlab.freedesktop.org/2A4U/openvpn-switcher#nmcli-etc-rclocal-startup-recommended-settings-sysvinit) `echo 'default via 1.2.3.4' > /dev/shm/default_route.log` where "default via 1.2.3.4" are the numbers obtained in step 1.

## Get default gateway ip info
`nswitch` writes the first default gateway to /dev/shm or ${vpntmpDirectory}/*default_route.log*.

Otherwise:
```
ip route list 0/0
```

## Stopping dhcp from readding a default gateway during vpn connection
In /etc/ … *dhcpcd.conf*:
```
nogateway
```
Other methods may vary by Linux distro.

## `nmcli` /etc/ … *rc.local* startup recommended settings (SysVinit)
```
echo 'default via 1.2.3.4' > /dev/shm/default_route.log # write gateway if dhcp is set to 'nogateway' in dhcpcd.conf
source /my_source_directory/openvpn-switcher/nswitcher.bash
#bindsys # redundant line if 'syspass' is on the following line
autoshift my_ovpn_directory resume save limit random nmanager syspass vpnroute firewall breakautoshift
```

## `openvpn` /etc/ … *rc.local* startup recommended settings (SysVinit)
```
source /my_source_directory/openvpn-switcher/nswitcher.bash
nswitch my_ovpn_directory resume save limit random background vpnroute firewall
```

## Encrypt/create *vpn_login.txt.**gpg***
```
gpg2 -c vpn_login.txt
```

## Create *Switch.desktop* icon

1. Rename *Switch-kdialog.desktop.template* to *Switch.desktop*.

2. Edit *Switch.desktop* and customize 'my_source_directory', 'my_ovpn_directory' and nswitch options.

3. `mv Switch.desktop $HOME/Desktop/` and `chmod u+x Switch.desktop`.

## *Switch_Firefox_Private.desktop* icon

*Switch_Firefox_Private_kdialog.desktop.template* closes a running instance of Firefox, switches to another VPN and restarts Firefox in Private Browsing mode. See steps in [Create *Switch.desktop* icon](https://gitlab.freedesktop.org/2A4U/openvpn-switcher#create-switchdesktop-icon) above to create a working instance.

## *Switch_Latency.desktop* icon

*Switch_Latency_kdialog.desktop.template* determines the host with the least latency. A host is randomly selected from a list of hosts with near equal latency. As the selection queue progresses the hosts increase in latency. [Edit this template like as above](https://gitlab.freedesktop.org/2A4U/openvpn-switcher#create-switchdesktop-icon).

## Todo - perhaps
- [ ] WireGuard option.
- [ ] recreate `nmcli` default ethernet device 
- [ ] systemd startup option.
- [ ] rewrite [openvpn-update-resolv-conf](https://github.com/alfredopalhares/openvpn-update-resolv-conf) scripts in pure bash/possibly fix experimental `tunnel` option.
- [ ] recall user login for limited time option.
- [ ] set default route to VPN server gateway option.
- [ ] set default route to VPN server IP option.
- [ ] allow application through tun (split tunnel).
- [ ] simultaneous tun connections (namespace per user?).
- [ ] learn how to and create a [GUI](https://forums.openvpn.net/viewtopic.php?f=15&t=21691).
- [x] nmcli retry.
- [ ] delay system start until vpn connection or user interaction.
    - [x] SysVinit
    - [ ] systemd
- [x] no reconnect unless connection log empty.
- [x] add nmcli switching + limit default route options.
- [ ] implement [Hardening OpenVPN Security](https://openvpn.net/community-resources/hardening-openvpn-security/).
- [x] use nftables to prevent ~~accidental disconnection from tun0~~ IP leak.
- [ ] add a menu or TUI (Text User Interface) option.
- [ ] add VPN chain linking.
- [ ] add time delayed auto switching option.
- [ ] add auto switching on VPN server down option.
- [ ] switch to specific *\*.ovpn* or folder list per application.
- [x] auto run after boot option.
- [ ] no network connection until $USER login option.
- [ ] restrict VPN to $USER option.
- [ ] auto switch and restrict to *\*.ovpn* folder or file per $USER's active virtual terminal or X display option.
- [ ] auto delete unresponsive *\*.ovpn*.
- [ ] auto download *\*.ovpn*.
- [x] test performance between pure bash ~~verses tr~~ and grep — pure bash is faster than grep when editing 2 or less lines — at editing 3 or more lines grep is faster.

## Experimental
Combine `nmcli` and `openvpn`:
```
tunnel my_ovpn_directory nm : my_ovpn_directory link
```

command|description
-|-
`autoswitch`¹ ⁴|detect exit status of `openvpn` then `nswitch` when VPN disconnects — invokes `nonpersistent` — syntax: `autoswitch my_ovpn_directory my_options`
`pkillauto`¹ ²|kill `autoswitch` process

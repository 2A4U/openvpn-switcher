#!/usr/bin/env bash
[[ "${BASH_SOURCE:0:1}" = '.' ]] && nswitcherDirectory="$PWD" || nswitcherDirectory="${BASH_SOURCE%/*}"
source "${nswitcherDirectory}/nswitcher.bash"
while ! nswitch "${@}" ; do : ; done

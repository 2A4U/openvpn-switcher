#!/usr/bin/env bash
[[ ${BASH_SOURCE:0:1} == '.' ]] && nswitcherDirectory="$PWD" || nswitcherDirectory="${BASH_SOURCE%/*}"
source "${nswitcherDirectory}/nswitcher_user_variables.bash"

ipv6() {
	[[ $1 == off ]] && { echo '1' >/proc/sys/net/ipv6/conf/all/disable_ipv6; }
	[[ $1 == on ]] && { echo '0' >/proc/sys/net/ipv6/conf/all/disable_ipv6; }
	local ipv6status
	readarray -t -n 1 ipv6status </proc/sys/net/ipv6/conf/all/disable_ipv6
	[[ $ipv6status == 1 ]] && {
		echo -e '\n \xE2\x9C\x98 ipv6 is \e[0;36mdisabled\e[0m.'
		return
	}
	[[ $ipv6status == 0 ]] && echo -e '\n \e[1;31m\xE2\x9A\xA0\e[0m ipv6 is \e[1;31menabled\e[0m.'
}

bindsys() {
	[[ $(findmnt -M /etc/NetworkManager/system-connections) ]] || {
	sudo mkdir -p /dev/shm/system-connections
	sudo mount --bind /dev/shm/system-connections /etc/NetworkManager/system-connections
	boundSystemConnectionsToShm='yes'
}; }

loadDefaultRoute() {
	readarray -t defaultRoute <<<$(ip route show)
	for originalRoute in "${!defaultRoute[@]}"; do [[ ${defaultRoute[originalRoute]/[[:space:]]*/} == 'default' && ! ${defaultRoute[originalRoute]} == *'vpn'* && ${defaultRoute[originalRoute]} != *'tun'* ]] && {
		[[ ! -e "${vpntmpDirectory}/default_route.log" ]] && echo "${defaultRoute[originalRoute]/[[:space:]]dev*/}" >${vpntmpDirectory}/default_route.log
		return
	}; done
	readarray -t defaultRoute <${vpntmpDirectory}/default_route.log
	sudo ip route add ${defaultRoute}
}

getActiveConnection() { readarray -s 1 -t activeConnection <<<$(nmcli --fields type,uuid connection show --active); }

nmreload() {
	getActiveConnection
	[[ ! -e "${vpntmpDirectory}/nm_active_general_default_uuid.log" ]] && { for getUUID in "${!activeConnection[@]}"; do [[ ${activeConnection[getUUID]} == *'ethernet'* ]] && {
		nmcli connection modify ${activeConnection[getUUID]/#*[[:space:]][[:space:]]/} ipv4.never-default true ipv6.never-default true
		nmcli connection down ${activeConnection[getUUID]/#*[[:space:]][[:space:]]/}
		nmcli connection up ${activeConnection[getUUID]/#*[[:space:]][[:space:]]/}
		break
	}; done; }
}

nmDown() {
	for getUUID in "${!activeConnection[@]}"; do [[ ${activeConnection[getUUID]} == *'vpn'* && -z $link ]] && nmcli connection down ${activeConnection[getUUID]/vpn/}; done
	[[ ${tmpvpnConfig} != "${latestvpn}" && -e "/etc/NetworkManager/system-connections/${tmpvpnConfig/.ovpn/.nmconnection}" && -z $link ]] && nmcli connection delete "${tmpvpnConfig/.ovpn/}"
}

killvpn() { if pidof openvpn; then sudo killall openvpn; fi; }

direct() {
	sudo nft flush ruleset
	getActiveConnection
	nmDown
	nmcli connection delete "${latestvpn/.ovpn/}" &>/dev/null
	killvpn
	loadDefaultRoute
	unset activeConnection getUUID tmpvpnConfig latestvpn defaultRoute
	echo -e '\n \e[1;31m\xE2\x9A\xA0\e[0m VPN \e[1;31mdown\e[0m. Direct connection.'
}

tunnel() {
	readarray -d ':' commandLineOptions <<<"$@"
	for singleConnection in "${!commandLineOptions[@]}"; do eval 'nswitch ${commandLineOptions[singleConnection]/:/} resume save limit random'; done
}

silenceLine() { echo -en "\e[1A\e[0K\r"; }

decryptGpg() {
	(umask 0066 && "$gpgProgramName" --quiet --decrypt "${1}/vpn-login.txt.gpg" >"${vpntmpDirectory}/vpn-login.txt" || {
		echo 'RELOADAGENT' | gpg-connect-agent &>/dev/null &
		unlink "${vpntmpDirectory}/vpn-login.txt" &
		exit 1
	}) || return
	[[ "$useNmcli" ]] || {
		sleep 1
		sudo unlink "${vpntmpDirectory}/vpn-login.txt"
	} &
	silenceLine
}

resume() {
	[[ -s "${connectLogDirectory}/connections_remaining${ovpnConfigDirectory//\//_}.log" ]] && {
	[[ ! -O "${connectLogDirectory}/connections_remaining${ovpnConfigDirectory//\//_}.log" ]] && {
		sudo chmod u+rw "${connectLogDirectory}/connections_remaining${ovpnConfigDirectory//\//_}.log"
		sudo chown ${USER}:$(id -gn) "${connectLogDirectory}/connections_remaining${ovpnConfigDirectory//\//_}.log"
	}
	readarray -t listovpn <"${connectLogDirectory}/connections_remaining${ovpnConfigDirectory//\//_}.log"
}; }

writeLatency() {
	local server configLines remoteLine ipAddress resolveHost ipArray latencyLog latency latencyArray
	for server in "${listovpn[@]}"; do
		readarray -t configLines <"$server"

		for remoteLine in "${!configLines[@]}"; do
			[[ ${configLines[remoteLine]} == 'remote '* ]] && {
				ipAddress="${configLines[remoteLine]#remote }"
				ipAddress="${ipAddress%[[:space:]][[:digit:]]*}"
				[[ $ipAddress != [[:digit:]]*.[[:digit:]]*.[[:digit:]]*.[[:digit:]]* ]] && {
					readarray -n 1 -t resolveHost <<<$(getent ahostsv4 "$ipAddress")
					ipAddress="${resolveHost[@]%%[[:space:]]*}"
				}
				ipArray+=($ipAddress)
				break
			}
		done
	done

	readarray -s 1 -t latencyLog <<<$(nmap -sn -n "${ipArray[@]}")
	echo "${latencyLog[-1]#*:[[:space:]]}"

	for ipAddress in "${!latencyLog[@]}"; do
		[[ $((ipAddress % 2)) == 0 && ${latencyLog[ipAddress]} == Nmap*[[:digit:]]*.[[:digit:]]*.[[:digit:]]*.[[:digit:]]* ]] && {
			latency="${latencyLog[ipAddress + 1]//[^[:digit:].]/}"
			latencyArray+=("${latency%.}")
			((ipAddress++))
		}
	done

	[[ "$saveConnectionsDown" ]] && {
		for ipAddress in "${!ipArray[@]}"; do [[ "${latencyArray[ipAddress]}" ]] || local hostsDown+=("${listovpn[ipAddress]}"); done
		printf '%s\n' "${hostsDown[@]}" >"${connectLogDirectory}/connections_down${ovpnConfigDirectory//\//_}.log"
	} || {
		readarray -t ascendingLatency <<<$(for ipAddress in "${!ipArray[@]}"; do
			${latencyArray[ipAddress]:+echo "${listovpn[ipAddress]},${latencyArray[ipAddress]}"}
		done | sort --numeric-sort --key='2' --field-separator=',')
	}
}
pkillauto() { pkill --pidfile /dev/shm/auto_switch_loop.bash.pid &>/dev/null; }

autoswitch() { ${nswitcherDirectory}/auto_switch_start.bash ${nswitcherDirectory}/auto_switch_loop.bash "${@}"; }

routeUp() {
	[[ ${routeUpOptions} == *"$1"* ]] || {
	routeUpOptions="${routeUpOptions:-route-up \"${nswitcherDirectory}/route_up_options.bash\"}"
	routeUpOptions="${routeUpOptions/%\"/ ${1}\"}"
}; }

autoshift() {
	[[ $@ == *'nmanager'* || $@ == *'nm'* ]] || {
		echo -e '\n \xE2\x9A\xA0 autoshift requires \e[0;36mnmanager\e[0m option.'
		return
	}
	local routeUpOptions shiftOption=("${@}") publicAddress isHostUp port connectAttempt scanCount='0' tryLimit="${tryLimit:-9}"
	routeUp autoshiftOn
	routeUp writePublicAddress

	for removeOption in "${!shiftOption[@]}"; do [[ ${shiftOption[removeOption + 1]} == [[:digit:]]* ]] && {
		if [[ ${shiftOption[removeOption]} == 'retry' ]]; then
			local retryOn="${shiftOption[removeOption + 1]}"
			unset shiftOption[removeOption] shiftOption[removeOption+1]
			shiftOption+=('reconnect' 'breakautoshift')
			set "${shiftOption[@]}"
			((removeOption++))
		elif [[ ${shiftOption[removeOption]} == 'max' ]]; then
			tryLimit="${shiftOption[removeOption + 1]}"
			unset shiftOption[removeOption] shiftOption[removeOption+1]
			set "${shiftOption[@]}"
			((removeOption++))
		fi
	}; done

	nswitch "$@"

	while :; do
		[[ -s ${vpntmpDirectory}/public_address.log ]] && {
			readarray -t publicAddress <${vpntmpDirectory}/public_address.log
			sudo unlink ${vpntmpDirectory}/public_address.log
		}
		[[ "$publicAddress" ]] && { [[ $@ == *'scanhost'* ]] && { readarray -t -s 2 -n 1 isHostUp <<<$(sudo nmap --disable-arp-ping -Pn "$publicAddress" -p"${port[@]/%/,}"); } || if ping -q -c 1 "$publicAddress" &>/dev/null; then isHostUp='Host is up.'; fi; }
		if [[ $isHostUp == 'Host is up.' ]]; then
			[[ $@ == *'breakautoshift'* ]] && break
			connectAttempt=()
			isHostUp=()
			silenceLine
			((scanCount++))
			echo -e " \xE2\x87\xB5 \e[0;36m${scanCount}\e[0m."
			sleep 11
		else
			[[ $connectAttempt -ge ${tryLimit} ]] && break
			((connectAttempt++))
			[[ $retryOn ]] && { [[ $retryOn -le 0 ]] && break || {
				((retryOn--))
				echo -e "\n \xE2\x99\xBB \e[0;36m$retryOn\e[0m tries remaining. Retrying in 3 seconds."
				sleep 3
			}; }
			port=()
			nswitch "$@"
		fi
	done
}

purge() {
	unset {userDefaultOvpnConfigDirectory,latestvpn,next,indexNumber,listovpn,tmpvpnConfig,vpnLoginTextDirectory,defaultRoute,ovpnConfigDirectory,boundSystemConnectionsToShm}
	pkillauto
	sudo rm -f ${connectLogDirectory}/connections_remaining*.log ${vpntmpDirectory}/*.ovpn ${vpntmpDirectory}/vpn-login.txt /dev/shm/auto_switch_loop.bash.pid &>/dev/null
	unset {connectLogDirectory,vpntmpDirectory}
	echo 'RELOADAGENT' | gpg-connect-agent
}

nswitch() {
	trap 'return' INT
	set +m
	unset latestvpn indexNumber
	listovpn=($(ls -c1 *.ovpn)) || silenceLine
	local previousOvpnConfigDirectory="$ovpnConfigDirectory"
	connectLogDirectory="${connectLogDirectory:-/dev/shm}"
	local userDefaultOvpnConfigDirectory="${userDefaultOvpnConfigDirectory:-$ovpnConfigDirectory}"
	local resolvOptions=('setenv PATH /usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin' 'script-security 2' "down $resolvConf" 'down-pre' 'remote-cert-tls server')
	# if pgrep -F '/dev/shm/auto_switch_loop.bash.pid' &> /dev/null ; then resolvOptions+=('ping 10' 'ping-exit 12' 'resolv-retry 0') ; fi # Force code.
	local gpgProgramName option ascendingLatency vpnLoginVariableDirectory activeConnection getUUID
	[[ $routeUpOptions ]] || local routeUpOptions port

	local removeVpnSettings=('script-security' 'up ' 'down ')
	vpntmpDirectory="${vpntmpDirectory:-/dev/shm}"
	gpgProgramName="${gpgProgramName:-gpg2}"

	if [[ -d $1 ]]; then
		cd "$1"
		ovpnConfigDirectory="$PWD"
		cd - &>/dev/null
		shift
	elif [[ ${listovpn[0]} == *'.ovpn'* ]]; then
		ovpnConfigDirectory="$PWD"
	else
		ovpnConfigDirectory="$userDefaultOvpnConfigDirectory"
	fi

	ovpnConfigDirectory="${ovpnConfigDirectory:-$nswitcherDirectory}"

	[[ $ovpnConfigDirectory != "$PWD" ]] && {
		cd "${ovpnConfigDirectory}"
		listovpn=($(ls -c1 *.ovpn))
		echo -en "\n Queue directory \e[36;32m\xe2\x96\xb6\e[0m $PWD \e[36;32m\xE2\x97\x80\e[0m\n"
		cd - &>/dev/null
	} ||
		echo -en "\n Queue directory \e[1;31m\xe2\x96\xb6\e[0m ${ovpnConfigDirectory} \e[1;31m\xE2\x97\x80\e[0m\n"

	cd "${vpntmpDirectory}"
	tmpvpnConfig=(*.ovpn)
	cd - &>/dev/null

	for option in "$@"; do case "$option" in
		remove | delete) sudo unlink "${ovpnConfigDirectory}/${tmpvpnConfig[0]}" &>/dev/null && echo -e "\n Previous config \e[1;30m${tmpvpnConfig[0]}\e[0m deleted." ;;
		restart | new | reset)
			unset tmpvpnConfig
			sudo rm "${vpntmpDirectory}"/*.ovpn
			;;
		killvpn | kill) killvpn ;;
		skip | next) local next='1' ;;
		*/*.ovpn)
			ovpnConfigDirectory="${option%/*}"
			latestvpn="${option##*/}"
			;;
		*.ovpn) latestvpn="$option" ;;
		reconnect | hold) [[ $tmpvpnConfig == '*.ovpn' ]] && echo -e '\n Previous connection not found \xe2\x80\x94 using new connection.' || latestvpn="$tmpvpnConfig" ;;
		login) local loginWithDefaultText='yes' ;;
		limit | minus | l)
			local minimizeAvailableConnections='yes'
			[[ $ovpnConfigDirectory == "$previousOvpnConfigDirectory" && ${minList[@]} ]] && listovpn=("${minList[@]}")
			[[ ! $ovpnConfigDirectory == "$previousOvpnConfigDirectory" ]] && resume
			;;
		resume) resume ;;
		save | savelog} | s) local saveRemainingConnections='yes' ;;
		[[:digit:]]*) [[ "${option//[[:digit:]]/}" ]] || latestvpn="${listovpn[option - 1]}" ;;
		random | r) local pseudoRandom='yes' ;;
		auto | background | bg) local backgroundProcess='yes' ;;
		purge) purge ;;
		nmanager | nm)
			local useNmcli='yes'
			routeUp cutDefaultRoute
			getActiveConnection
			;;
		nmreload) nmreload ;;
		bindsys | nmshm) bindsys ;;
		syspass)
			local systemPassword='yes'
			[[ "$boundSystemConnectionsToShm" ]] || bindsys
			;;
		latency | ping) resume ; declare {ascendingLatency,minimizeAvailableConnections,saveRemainingConnections}='yes' ;;
		nologhost | nl) local noSaveConnectionsUp='yes' ;;
		loghostdown | lh)
			local saveConnectionsDown='yes'
			cd "$ovpnConfigDirectory"
			writeLatency
			cd - &>/dev/null
			return
			;;
		deletehosts)
			readarray -t deleteHostsList <"${connectLogDirectory}/connections_down${ovpnConfigDirectory//\//_}.log"
			rm -v "${deleteHostsList[@]/#/$ovpnConfigDirectory/}" "${connectLogDirectory}/connections_down${ovpnConfigDirectory//\//_}.log"
			return
			;;
		nonpersistent | np) local nonPersistent='yes' ;; #resolvOptions+=( 'pull-filter ignore ping' ) # Force code.
		applist | kl)
			local appList killAppList
			killAppList='yes'
			[[ -s "${nswitcherDirectory}/app_list.txt" ]] && readarray -t appList <"${nswitcherDirectory}/app_list.txt"
			;;
		nobody | nogroup | dp) resolvOptions+=('user nobody' 'group nogroup') ;;
		noresolvretry | nr) resolvOptions+=('resolv-retry 0') ;;
		vpnroute | dr) routeUp cutDefaultRoute ;;
		firewall | fw)
			routeUp writePublicAddress
			routeUp firewallOn
			;;
		showip | ip)
			routeUp writePublicAddress
			routeUp showIP
			;;
		breakautoshift) ;;
		scanhost) ;;
		link) local link='yes' ;;
		udp) local udp='yes' ;;
		tcp) local tcp='yes' ;;
		*) if [[ "$killAppList" ]] && type -p "$option" &>/dev/null; then
			appList+=("$option")
		else
			echo -e "\n \e[5m\xE2\x9A\xA0\e[0m Option '\e[1;31m${option}\e[0m' not valid. Check spelling, command syntax or current directory (\e[0;3m${PWD}\e[0m).\n"
			tryLimit="${tryLimit:+0}"
			return
		fi ;;
		esac done

	${killAppList:+sudo killall "${appList[@]}"} &>/dev/null

	[[ -e "${vpntmpDirectory}/rules.nft" || -e "${vpntmpDirectory}/public_address.log" ]] && sudo rm "${vpntmpDirectory}/rules.nft" "${vpntmpDirectory}/public_address.log" &>/dev/null
	sudo nft flush ruleset

	[[ "$link" ]] || {
		loadDefaultRoute
		resolvOptions+=("up $resolvConf")
	} #direct

	if [[ $ascendingLatency == 'yes' && ${listovpn[0]} != *','* ]]; then
		echo -en "\n Pinging servers..."
		cd "$ovpnConfigDirectory"
		writeLatency
		cd - &>/dev/null
		listovpn=("${ascendingLatency[@]}")
		latestvpn="${listovpn[0]%,*}"
		[[ $noSaveConnectionsUp && -e "${connectLogDirectory}/connections_up${ovpnConfigDirectory//\//_}.log" ]] || (
			umask 0066
			printf '%s\n' "${listovpn[@]}" >"${connectLogDirectory}/connections_up${ovpnConfigDirectory//\//_}.log"
		)
	fi

	[[ "$pseudoRandom" ]] && { if [[ $ascendingLatency && ${listovpn[0]} == *','* ]]; then
		{
			indexNumber='0'
			until [[ ${listovpn[indexNumber]#*,} != "${listovpn[0]#*,}" ]]; do ((indexNumber++)); done
			latestvpn="${listovpn[RANDOM % indexNumber]%,*}"
		}
	else
		latestvpn="${listovpn[(RANDOM % ${#listovpn[@]}) + 1]%,*}"
	fi; }

	[[ -z ${tmpvpnConfig[@]} || ${tmpvpnConfig[0]} == '*.ovpn' ]] || {
		for indexNumber in "${!listovpn[@]}"; do [[ ${listovpn[indexNumber]%,*} == "${tmpvpnConfig[0]}" ]] && break; done
		latestvpn="${latestvpn:-${listovpn[indexNumber + 1 + next]%,*}}"
		[[ "$link" ]] || sudo rm "${vpntmpDirectory}"/*.ovpn
	}

	latestvpn="${latestvpn:-${listovpn[0 + next]%,*}}"
	[[ "$latestvpn" ]] || return

	cd "${ovpnConfigDirectory}"
	readarray -t editovpn <<<"$(tr -d '\015' <${latestvpn})"

	[[ "$nonPersistent" ]] && removeVpnSettings+=(persist-tun persist-key resolv-retry)
	for delLine in "${!editovpn[@]}"; do
		[[ ${editovpn[delLine]} == remote* ]] && port+=("${editovpn[delLine]//*[[:space:]]/}")
		for setting in "${!removeVpnSettings[@]}"; do
			[[ $udp && ${editovpn[delLine]} == 'proto tcp' ]] && {
				editovpn[delLine]='proto udp'
				echo -e '\n "proto \e[0;36mtcp\e[0m" config detected \xe2\x80\x94 trying \e[36;32mudp\e[0m connection.\n'
				break
			}
			[[ $tcp && ${editovpn[delLine]} == 'proto udp' ]] && {
				editovpn[delLine]='proto tcp'
				echo -e '\n "proto \e[0;36mudp\e[0m" config detected \xe2\x80\x94 trying \e[36;32mtcp\e[0m connection.\n'
				break
			}
			[[ ${editovpn[delLine]} == *"${removeVpnSettings[setting]}"* ]] && {
				unset editovpn[delLine] removeVpnSettings[setting]
				break
			}
		done
		[[ "${removeVpnSettings[@]}" ]] || break
	done

	if [[ -e vpn-login.txt ]]; then
		vpnLoginVariableDirectory="$PWD"
	elif [[ -e vpn-login.txt.gpg ]]; then
		vpnLoginVariableDirectory="${vpntmpDirectory}"
		decryptGpg "$PWD" || {
			cd - &>/dev/null
			return
		}
	elif [[ "$loginWithDefaultText" ]]; then
		echo -e "\n \e[1;31mWarning\e[0m \xe2\x80\x94 sending login info from ${vpnLoginTextDirectory}/vpn-login.txt (or *.gpg) \xe2\x80\x94 verify if '\e[0;36m${latestvpn}\e[0m' VPN is the intended target.\n"
		read -n 1 -p $' Press any key to continue \xe2\x80\x94 [Esc] to exit...\n'
		case "$REPLY" in $'\e')
			unset ovpnConfigDirectory
			cd - &>/dev/null
			return
			;;
		esac
		[[ -e "${vpnLoginTextDirectory}/vpn-login.txt.gpg" ]] && { decryptGpg "${vpnLoginTextDirectory}" || {
			cd - &>/dev/null
			return
		}; } && vpnLoginVariableDirectory="${vpntmpDirectory}" || vpnLoginVariableDirectory="${vpnLoginTextDirectory}"
	fi

	[[ "$vpnLoginVariableDirectory" ]] && { [[ "$useNmcli" ]] && {
		readarray -t loginText <"${vpnLoginVariableDirectory}/vpn-login.txt"
		(umask 0066 && echo "vpn.secrets.password:${loginText[@]:1:1}" >"${vpntmpDirectory}/nmlogin.txt")
		local nmPass="${vpntmpDirectory}/nmlogin.txt"
	} || editovpn=("${editovpn[@]/auth-user-pass/auth-user-pass ${vpnLoginVariableDirectory}/vpn-login.txt}"); }

	(umask 0066 && printf '%s\n' "${editovpn[@]}" "${resolvOptions[@]}" "${routeUpOptions[@]}" >"${vpntmpDirectory}/${latestvpn}")

	for indexNumber in "${!listovpn[@]}"; do [[ ${listovpn[indexNumber]%,*} == "${latestvpn}" ]] && break; done

	[[ ${listovpn[indexNumber]#*,} == [[:digit:]]* ]] && ascendingLatency=" \xe2\x97\x94 \e[0;36m${listovpn[indexNumber]#*,}\e[0ms"

	echo -e "\n \e[0;36m$((indexNumber + 1))\e[0m/${#listovpn[@]} \xe2\x80\x94 Connecting to \e[0;36m\xe2\x96\xb6\e[0m ${latestvpn} \e[0;36m\xE2\x97\x80\e[0m${ascendingLatency}\n"

	[[ "minimizeAvailableConnections" ]] && {
		unset 'listovpn[indexNumber]'
		minList=("${listovpn[@]}")
		if [[ ${#minList} -le 1 ]]; then sudo unlink "${connectLogDirectory}/connections_remaining${PWD//\//_}.log"; else
			[[ "$saveRemainingConnections" ]] && (
				umask 0066
				printf '%s\n' "${minList[@]}" >"${connectLogDirectory}/connections_remaining${PWD//\//_}.log"
			)
		fi
	}

	if [[ "$useNmcli" ]]; then
		[[ ${tmpvpnConfig} != "${latestvpn}" ]] && nmcli connection import --temporary type openvpn file "${vpntmpDirectory}/${latestvpn}"

		nmcli connection modify "${latestvpn/.ovpn/}" ipv4.dns-priority '-1' ipv6.method 'disabled'

		[[ "$nonPersistent" ]] || nmcli connection modify "${latestvpn/.ovpn/}" vpn.persistent true

		[[ "$systemPassword" ]] && nmcli connection modify "${latestvpn/.ovpn/}" +vpn.data password-flags=0 +vpn.secrets password=${loginText[@]:1:1}

		nmDown
		[[ "$link" ]] || { killvpn; }

		{
			sleep 1
			[[ -e "${vpntmpDirectory}/vpn-login.txt" ]] && rm "$nmPass" "${vpntmpDirectory}/vpn-login.txt" &>/dev/null
			unset loginText nmPass
			nmcli connection modify ${latestvpn/.ovpn/} vpn.user-name null vpn.secrets password=null &
		} &
		silenceLine
		silenceLine

		if [[ "$nmPass" ]]; then
			nmcli connection modify "${latestvpn/.ovpn/}" vpn.user-name "${loginText[@]:0:1}"
			nmcli --wait 10 connection up ${latestvpn/.ovpn/} passwd-file "$nmPass"
		else
			nmcli --wait 10 connection up "${latestvpn/.ovpn/}"
		fi

		echo ''
		${routeUpOptions:10:-1}

		set -m
		cd - &>/dev/null
		echo ''

	else
		nmDown
		[[ "$link" ]] || killvpn

		if [[ "$backgroundProcess" ]]; then { sudo openvpn --connect-retry-max 0 "${vpntmpDirectory}/${latestvpn}" & } else
			sudo openvpn "${vpntmpDirectory}/${latestvpn}"
			## Test exit status code.
			if [[ $? == 1 ]]; then
				echo -e "\e[1;32m openvpn exits on its own with error status from remote VPN failure\e[0m"
				set -m
				cd - &>/dev/null
				return 1
			else
				echo -e "\e[1;31m openvpn exits with success. return 1 needs to be forced.\e[0m"
				set -m
				cd - &>/dev/null
				return 1
			fi
			## Force code.
			#set -m ; cd - &> /dev/null ; [[ "${#resolvOptions[@]}" -gt 5 ]] && return 1
		fi
	fi
	wait &>/dev/null
}
